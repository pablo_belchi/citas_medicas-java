package com.example.Citas_Medicas;

import javax.sql.DataSource;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration.AccessLevel;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@EnableJpaRepositories(basePackages="com.example.Citas_Medicas.Repository")
@EnableTransactionManagement
public class ApplicationConfig {

	//Contexto para persistencia:
	@Bean(name="DataSource")
	public DataSource datasource() {
		
		DriverManagerDataSource OrigenDatos = new DriverManagerDataSource();
		OrigenDatos.setPassword("meloinvento");
		OrigenDatos.setUsername("SYSTEM");
		OrigenDatos.setUrl("jdbc:oracle:thin:@192.168.200.108:1521:XE");
		OrigenDatos.setDriverClassName("oracle.jdbc.OracleDriver");
		
		return OrigenDatos;
	}
	
	//Unidad de persistencia
	@Bean(name="entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean Emf() {
		
		LocalContainerEntityManagerFactoryBean localContainer = new LocalContainerEntityManagerFactoryBean();
		localContainer.setDataSource(datasource());
		localContainer.setPackagesToScan("com.example.Citas_Medicas.Modelo");
		
		//Proveedor de persistencia
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		localContainer.setJpaVendorAdapter(vendorAdapter);
		
		
		return localContainer;
	}
	
	@Bean(name="transactionManager")
	public PlatformTransactionManager ptm() {
		JpaTransactionManager TransactionManager = new JpaTransactionManager(Emf().getObject());
		return TransactionManager;
	}
	
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper m = new ModelMapper();
		m.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT)
		.setFieldMatchingEnabled(true)
		.setFieldAccessLevel(AccessLevel.PRIVATE);
		
	    return m;
	}
	
}