package com.example.Citas_Medicas.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Citas_Medicas.DTO.CitaDTO;
import com.example.Citas_Medicas.DTO.NewCitaDTO;
import com.example.Citas_Medicas.DTO.OnlyCitaDTO;
import com.example.Citas_Medicas.Modelo.Cita;
import com.example.Citas_Medicas.Service.Cita_Service;

@RestController
@RequestMapping(path="/Cita")
public class Cita_Controller {

	@Autowired
	private Cita_Service Service_;
	@Autowired
	private ModelMapper Mapper_;
	
	@RequestMapping(path="/GetAll", method=RequestMethod.GET)
	public List<CitaDTO> GetAll(){
		
		List<Cita> list = Service_.GetAll();
		
		List<CitaDTO> listDto = new ArrayList<CitaDTO>();
		
		for(Cita l: list) {
			CitaDTO dto = Mapper_.map(l, CitaDTO.class);
			listDto.add(dto);
		}
		return listDto;
		
	}
	
	@RequestMapping(value="/GetById",method=RequestMethod.GET)
	public CitaDTO GetById(@RequestParam (value="Id")Long Id) {
		Cita c = Service_.GetById(Id);
		CitaDTO dto = Mapper_.map(c, CitaDTO.class);
		
		return dto;
	}
	
	@RequestMapping(path="/Save", method=RequestMethod.POST)
	public void SaveEntity(@RequestBody NewCitaDTO cDto) {
		Cita c = Mapper_.map(cDto, Cita.class);
		Service_.saveEntity(c);
		
	}
	
	@RequestMapping(value="/DeleteById",method=RequestMethod.DELETE)
	public void DeleteById(@RequestParam (value="Id") Long Id) { Service_.DeleteById(Id); }
	
	@RequestMapping(value="/Modify",method=RequestMethod.PUT)
	public void Modify(@RequestBody OnlyCitaDTO cDto) {
		Cita c = Mapper_.map(cDto, Cita.class);
		Service_.Modify(c);
	}
}