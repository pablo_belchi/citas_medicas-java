package com.example.Citas_Medicas.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Citas_Medicas.DTO.DiagnosticoDTO;
import com.example.Citas_Medicas.DTO.NewDiagnostico;
import com.example.Citas_Medicas.Modelo.Diagnostico;
import com.example.Citas_Medicas.Service.Diagnostico_Service;

@RestController
@RequestMapping(path="/Diagnostico")
public class Diagnostico_Controller {
	
	@Autowired
	private Diagnostico_Service Service_;
	@Autowired
	private ModelMapper Mapper_;

	@RequestMapping(path="/GetAll", method=RequestMethod.GET)
	public List<DiagnosticoDTO> GetAll(){ 
		List<Diagnostico> List = Service_.GetAll();
		List<DiagnosticoDTO> dtoList = new ArrayList<DiagnosticoDTO>();
		
		for(Diagnostico l: List) {
			DiagnosticoDTO dto = Mapper_.map(l, DiagnosticoDTO.class);
			dtoList.add(dto);
			
		}
		return dtoList; 
		
	}
	
	@RequestMapping(value="/GetById",method=RequestMethod.GET)
	public DiagnosticoDTO GetById(@RequestParam("Id") Long Id) {
		Diagnostico d = Service_.GetById(Id);
		DiagnosticoDTO dto = Mapper_.map(d, DiagnosticoDTO.class);
		
		return dto;
	}
	
	@RequestMapping(value="/DeleteById",method=RequestMethod.DELETE)
	public void DeleteById(@RequestParam("Id") Long Id) {
		Service_.DeleteById(Id);
	}
	
	@RequestMapping(value="/Modify",method=RequestMethod.PUT)
	public void DiagnosticarCita(@RequestBody NewDiagnostico dto) {
		Diagnostico d = Mapper_.map(dto, Diagnostico.class);
		Service_.Modify(d);
	}
	
}
