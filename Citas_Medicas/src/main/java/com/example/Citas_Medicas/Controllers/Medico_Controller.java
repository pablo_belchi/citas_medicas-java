package com.example.Citas_Medicas.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Citas_Medicas.DTO.MedicoDTO;
import com.example.Citas_Medicas.DTO.NewMedicoDTO;
import com.example.Citas_Medicas.Modelo.Medico;
import com.example.Citas_Medicas.Service.Medico_Service;

@RestController
@RequestMapping(path="/Medico")
public class Medico_Controller {

	@Autowired 
	private Medico_Service Service_;
	@Autowired
	private ModelMapper Mapper;
	
	@RequestMapping(value="/GetAll",method=RequestMethod.GET)
	public List<MedicoDTO> GetAll(){ 
		List<Medico> lista = Service_.GetAll();
		List<MedicoDTO> dtoList = new ArrayList<MedicoDTO>();
		
		for(Medico m: lista) {
			dtoList.add(Mapper.map(m, MedicoDTO.class));
		}
		
		return dtoList;
	}
	
	@RequestMapping(value="/GetById",method=RequestMethod.GET)
	public MedicoDTO GetById(@RequestParam (value="Id") Long Id) { 
		Medico m = Service_.GetById(Id);
		MedicoDTO dto = Mapper.map(m, MedicoDTO.class);
		
		return dto;
	}
	
	@RequestMapping(value="/DeleteById",method=RequestMethod.DELETE)
	public void DeleteByid(@RequestParam (value="Id") Long Id) { Service_.DeleteById(Id); }
	
	@RequestMapping(value="/Save",method=RequestMethod.POST)
	public void SaveEntity(@RequestBody NewMedicoDTO m) { 
		Medico md = Mapper.map(m, Medico.class);
		Service_.saveEntity(md);
		
	}
	
	@RequestMapping(value="/Modify",method=RequestMethod.PUT)
	public void Modify(@RequestBody NewMedicoDTO m) { 
		Medico md = Mapper.map(m, Medico.class);
		Service_.Modify(md); 
		
	}	
}