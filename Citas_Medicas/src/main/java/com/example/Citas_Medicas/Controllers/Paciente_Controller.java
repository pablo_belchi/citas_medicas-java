package com.example.Citas_Medicas.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Citas_Medicas.DTO.NewPacienteDTO;
import com.example.Citas_Medicas.DTO.PacienteDTO;
import com.example.Citas_Medicas.Modelo.Paciente;
import com.example.Citas_Medicas.Service.Paciente_Service;

@RestController
@RequestMapping(path="/Paciente")
public class Paciente_Controller {

	@Autowired 
	private Paciente_Service Service_;
	@Autowired
	private ModelMapper Mapper;
	
	@RequestMapping(value="/GetAll",method=RequestMethod.GET)
	public List<PacienteDTO> GetAll(){ 
		
		List<Paciente> lista = Service_.GetAll();
		List<PacienteDTO> listaDTO= new ArrayList<PacienteDTO>();
		
		for(Paciente u: lista) {
		 PacienteDTO dto = Mapper.map(u, PacienteDTO.class);
			
			listaDTO.add(dto);
		}
		
		return listaDTO;
	}
	
	@RequestMapping(value="/GetById",method=RequestMethod.GET)
	public PacienteDTO GetById(@RequestParam (value="Id") Long Id) { 
		Paciente p = Service_.GetById(Id);
		PacienteDTO dto = Mapper.map(p, PacienteDTO.class);
		
		return dto;
	}
	
	@RequestMapping(value="/DeleteById",method=RequestMethod.DELETE)
	public void DeleteByid(@RequestParam (value="Id") Long Id) { Service_.DeleteById(Id); }
	
	@RequestMapping(value="/Save",method=RequestMethod.POST)
	public void SaveEntity(@RequestBody NewPacienteDTO pdto) { 
		Paciente p = Mapper.map(pdto, Paciente.class);
		Service_.saveEntity(p); 
		
	}
	
	@RequestMapping(value="/Modify",method=RequestMethod.PUT)
	public void Modify(@RequestBody NewPacienteDTO p) { 
		Paciente pdto = Mapper.map(p, Paciente.class);
		Service_.Modify(pdto); 
		
	}	
}