package com.example.Citas_Medicas.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Citas_Medicas.DTO.NewUsuarioDTO;
import com.example.Citas_Medicas.DTO.UsuarioDTO;
import com.example.Citas_Medicas.Modelo.Usuario;
import com.example.Citas_Medicas.Service.Usuario_Service;

@RestController
@RequestMapping(path="/Usuario")
public class Usuario_Controller {

	@Autowired 
	private Usuario_Service Service_;
	@Autowired
	private ModelMapper Mapper;
	
	@RequestMapping(value="/GetAll",method=RequestMethod.GET)
	public List<UsuarioDTO> GetAll(){ 
		
		List<Usuario> lista = Service_.GetAll();
		List<UsuarioDTO> listaDTO= new ArrayList<UsuarioDTO>();
		
		for(Usuario u: lista) {
			UsuarioDTO dto = Mapper.map(u, UsuarioDTO.class);
			
			listaDTO.add(dto);
		}
		
		return listaDTO;
	}
	
	@RequestMapping(value="/GetById",method=RequestMethod.GET)
	public UsuarioDTO GetById(@RequestParam (value="Id") Long Id) { 
		Usuario u = Service_.GetById(Id);
		UsuarioDTO dto = Mapper.map(u, UsuarioDTO.class);
		return dto;
	}
	
	@RequestMapping(value="/DeleteById",method=RequestMethod.DELETE)
	public void DeleteByid(@RequestParam (value="Id") Long Id) { Service_.DeleteById(Id); }
	
	@RequestMapping(value="/Modify",method=RequestMethod.PUT)
	public void Modify(@RequestBody NewUsuarioDTO u) { 
		Usuario usr = Mapper.map(u, Usuario.class);
		Service_.Modify(usr); 
	}
}