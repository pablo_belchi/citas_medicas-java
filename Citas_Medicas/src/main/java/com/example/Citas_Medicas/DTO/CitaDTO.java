package com.example.Citas_Medicas.DTO;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class CitaDTO extends NewCitaDTO{
	private Long Cita_Id;
	//public DiagnosticoDTO Diagnostico;
	private OnlyMedicoDTO Medico;
	private PacienteDTO Paciente;
	
}