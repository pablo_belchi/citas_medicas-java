package com.example.Citas_Medicas.DTO;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class MedicoDTO extends UsuarioDTO{
	private String numColegiado;
	private List<OnlyCitaDTO> Cita;
	
}