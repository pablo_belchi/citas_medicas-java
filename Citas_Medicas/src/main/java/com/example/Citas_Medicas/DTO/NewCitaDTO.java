package com.example.Citas_Medicas.DTO;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class NewCitaDTO {
	private Calendar fechaHora;
	private String motivoCita;
	private int attribute11;
	private long MedicoId;
	private long PacienteId;

}
