package com.example.Citas_Medicas.DTO;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class NewDiagnostico {
	private Long Id;
	private String valoracionEspecialista;
	private String enfermedad;
}
