package com.example.Citas_Medicas.DTO;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class OnlyMedicoDTO extends UsuarioDTO{
	private String numColegiado;
}
