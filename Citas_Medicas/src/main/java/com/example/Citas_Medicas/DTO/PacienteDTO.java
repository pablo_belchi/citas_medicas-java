package com.example.Citas_Medicas.DTO;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class PacienteDTO extends UsuarioDTO {
	private String NSS;
	private String NumTarjeta;
	private String telefono;
	private String direccion;
}
