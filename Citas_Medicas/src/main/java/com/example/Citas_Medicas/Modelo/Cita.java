package com.example.Citas_Medicas.Modelo;

import java.io.Serializable;
import java.util.*;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@Table(name="CITA")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Cita implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CITA_ID")
	private Long Cita_Id;
	
	@Column(name="FECHAHORA", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar fechaHora;
	
	@Column(name="MOTIVOCITA", nullable = false)
	private String motivoCita;
	
	@Column(name="ATTRIBUTE11", nullable = false)
	private int attribute11;
	
	@Column(name="MEDICOID")
	private Long MedicoId;
	
	@Column(name="PACIENTEID")
	private Long PacienteId;
	
	//RELACIONES ENTRE TABLAS
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PACIENTEID", insertable=false, updatable=false)
	private Paciente Paciente;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MEDICOID", insertable=false, updatable=false)
	private Medico Medico;
	@OneToOne(fetch=FetchType.EAGER, mappedBy="Cita", cascade = CascadeType.REMOVE)
	private Diagnostico Diagnostico;
	
	//CONSTRUCTORES
	@PersistenceConstructor
	public Cita(Long Id_,int agno, int mes, int dia, int hora, int minuto, String motivoCita_, int attribute11_, 
			Long MedicoId_, Long PacienteId_) {
		
		this.fechaHora=new GregorianCalendar(agno, mes, dia, hora, minuto, 0);
		this.motivoCita=motivoCita_;
		this.attribute11=attribute11_;
		this.Cita_Id=Id_;
		
		this.MedicoId=MedicoId_;
		this.PacienteId=PacienteId_;
	}
	
	public Cita() {}

	//METODOS DE COMPARACIÓN POR ID
	@Override
	public int hashCode() { return Objects.hash(Get_Id()); }

	@Override
	public boolean equals(Object Obj) {
		
		if (Obj==null) {
			return false;
		}else if(getClass() != Obj.getClass()) {
			return false;
		}else{
			Cita elemento = (Cita)Obj;
			
			if(elemento.Get_Id() != Get_Id()) {
				return false;
			}else {
				return true;
			}
		}
	}
	
	//GETTERS
	public Long Get_Id() { return Cita_Id;}
	public Long Get_MedicoId() { return MedicoId;}
	public Long Get_PacienteId() { return PacienteId;}
	public Calendar Get_fechaHora() { return fechaHora;}
	public String Get_motivoCita() { return motivoCita;}
	public int Get_attribute11() { return attribute11;}
	
	//SETTERS
	public void Set_fechaHora(int agno, int mes, int dia, int hora, int minuto) { this.fechaHora=new GregorianCalendar(agno, mes, dia, hora, minuto);}
	public void Set_motivoCita(String motivoCita_) { this.motivoCita=motivoCita_;}
	public void Set_attribute11(int attribute11_) { this.attribute11=attribute11_;}
	public void Set_MedicoId(Long Id) { MedicoId=Id;}
	public void Set_PacienteId(Long Id) { PacienteId=Id;}
}
