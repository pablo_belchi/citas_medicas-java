package com.example.Citas_Medicas.Modelo;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@Table(name="DIAGNOSTICO")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Diagnostico implements Serializable {

	@Id
	@Column(name="ID")
	private Long Id;
	
	@Column(name="VALORACIONESPECIALISTA")
	private String valoracionEspecialista;
	
	@Column(name="ENFERMEDAD")
	private String enfermedad;
	
	//RELACIONES ENTRE TABLAS
	@OneToOne(cascade = CascadeType.REMOVE, fetch=FetchType.EAGER)
	@JoinColumn(name="ID")
	private Cita Cita;
	
	//CONSTRUCTORES
	@PersistenceConstructor
	public Diagnostico(long Id_, String valoracionEspecialista_, String enfermedad_, long CitaId_) {
		
		this.valoracionEspecialista=valoracionEspecialista_;
		this.enfermedad=enfermedad_;
		this.Id=Id_;
	}
	
	public Diagnostico() {}
	
	//METODOS DE COMPARACIÓN POR ID
	@Override
	public int hashCode() { return Objects.hash(Get_Id()); }

	@Override
	public boolean equals(Object Obj) {
		
		if (Obj==null) {
			return false;
		}else if(getClass() != Obj.getClass()) {
			return false;
		}else{
			Diagnostico elemento = (Diagnostico)Obj;
			
			if(elemento.Get_Id() != Get_Id()) {
				return false;
			}else {
				return true;
			}
		}
	}
	
	//GETTERS 
	public Long Get_Id() { return Id;}
	public String Get_valoracionEspecialista() { return valoracionEspecialista;}
	public String Get_enfermedad() { return enfermedad;}
	
	//SETTERS
	public void Set_valoracionEspecialista(String valoracionEspecialista_) { this.valoracionEspecialista=valoracionEspecialista_;}
	public void Set_enfermedad(String enfermedad_) { this.enfermedad=enfermedad_;}
}