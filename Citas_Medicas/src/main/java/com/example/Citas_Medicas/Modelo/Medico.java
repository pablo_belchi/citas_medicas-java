package com.example.Citas_Medicas.Modelo;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@Table(name="MEDICO")
@PrimaryKeyJoinColumn(name="ID")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Medico extends Usuario  {
	
	@Column(name="NUMCOLEGIADO", nullable = false)
	private String numColegiado;
	
	//RELACIONES ENTRE TABLAS
	@ManyToMany
	@JoinTable(name="MEDICOPACIENTE")
	private List<Paciente> Paciente;
	@OneToMany(mappedBy="Medico", cascade = CascadeType.REMOVE)
	private List<Cita> Cita;
	
	//CONSTRUCTORES
	@PersistenceConstructor
	public Medico(long Id_, String nombre_, String apellidos_, String usuario_, String clave_, String numColegiado_) {
		super(Id_, nombre_, apellidos_, usuario_, clave_);
		
		this.numColegiado=numColegiado_;
		this.Id=Id_;
		
	}
	
	public Medico() {}
	
	//GETTERS
	public String Get_numColegiado() { return this.numColegiado;}
	public List<Paciente> Get_Pacientes(){ return this.Paciente;}
	public List<Cita> Get_Citas(){ return this.Cita;}
	
	
	//SETTERS
	public void Set_numColegiado(String numColegiado_) { this.numColegiado= numColegiado_;}
	public void Set_Citas(List<Cita> Cita){ this.Cita =Cita; }
	public void Set_Pacientes(List<Paciente> paciente) { this.Paciente=paciente; }
	
}