package com.example.Citas_Medicas.Modelo;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@Entity
@Table(name="PACIENTE")
@PrimaryKeyJoinColumn(name="ID")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Paciente extends Usuario {

	@Column(name="NSS", nullable = false)
	private String NSS;
	
	@Column(name="NUMTARJETA", nullable = false)
	private String NumTarjeta;
	
	@Column(name="TELEFONO", nullable = false)
	private String telefono;
	
	@Column(name="DIRECCION", nullable = false)
	private String direccion;
	
	//RELACIONES ENTRE TABLAS
	@ManyToMany
	@JoinTable(name="MEDICOPACIENTE")
	private List<Medico> Medico;
	@OneToMany(mappedBy="Paciente",cascade = CascadeType.REMOVE)
	private List<Cita> Cita;
	
	//CONSTRUCTORES
	@PersistenceConstructor
	public Paciente(long Id_, String nombre_, String apellidos_, String usuario_, String clave_,String NSS_, String NumTarjeta_, String telefono_, String direccion_) {
		super(Id_, nombre_, apellidos_, usuario_, clave_);
		
		this.NSS=NSS_;
		this.NumTarjeta=NumTarjeta_;
		this.telefono=telefono_;
		this.direccion=direccion_;
		
	}
	
	public Paciente() {}
	
	//GETTERS
	public String Get_NSS() { return NSS;}
	public String Get_NumTarjeta() { return NumTarjeta;}
	public String Get_telefono() { return telefono;}
	public String Get_direccion() { return direccion;}
	
	//SETTERS
	public void Set_NSS(String NSS_) { this.NSS=NSS_;}
	public void Set_NumTarjeta(String NumTarjeta_) { this.NumTarjeta=NumTarjeta_;}
	public void Set_telefono(String telefono_) { this.telefono=telefono_;}
	public void Set_direccion(String direccion_) { this.direccion=direccion_;}
}
