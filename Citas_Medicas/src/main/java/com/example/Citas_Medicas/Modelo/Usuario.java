package com.example.Citas_Medicas.Modelo;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="USUARIO")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class Usuario implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	protected long Id;
	   
	@Column(name="NOMBRE", nullable = false)
	private String nombre;
	
	@Column(name="APELLIDOS", nullable = false)
	private String apellidos;
	
	@Column(name="USUARIO", nullable = false)
	private String usuario;
	
	@Column(name="CLAVE", nullable = false)
	private String clave;
	
	
	//CONSTRUCTORES
	@PersistenceConstructor
	public Usuario(long Id_, String nombre_, String apellidos_, String usuario_, String clave_){
	
		this.nombre=nombre_;
		this.apellidos=apellidos_;
		this.usuario=usuario_;
		this.clave=clave_;
		this.Id=Id_;
		
	}
	
	public Usuario() {}
	
	//METODOS DE COMPARACIÓN POR ID
	@Override
	public int hashCode() { return Objects.hash(Get_Id()); }

	@Override
	public boolean equals(Object Obj) {
		
		if (Obj==null) {
			return false;
		}else if(getClass() != Obj.getClass()) {
			return false;
		}else{
			Usuario elemento = (Usuario)Obj;
			
			if(elemento.Id != Id) {
				return false;
			}else {
				return true;
			}
		}
	}
	
	//GETTERS
	public long Get_Id() { return this.Id;}
	public String Get_nombre(){ return this.nombre;}
	public String Get_apellidos() { return this.apellidos;}
	public String Get_usuario() { return this.usuario;}
	public String Get_clave() { return this.clave;}
	
	//SETTERS
	//public void Set_Id(long Id) { this.Id=Id; } 
	public void Set_nombre(String nombre_) { this.nombre=nombre_;}
	public void Set_apellidos(String apellidos_) { this.apellidos=apellidos_;}
	public void Set_usuario(String usuario_) { this.usuario=usuario_;}
	public void Set_clave(String clave_) { this.clave=clave_;}
}