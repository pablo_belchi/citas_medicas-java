package com.example.Citas_Medicas.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.Citas_Medicas.Modelo.Cita;

@Repository
public interface CitaDAO extends JpaRepository<Cita, Long> {
	
	@Query("select Cita_Id from Cita where ROWNUM <= 1 ORDER BY Cita_Id DESC")
	public Long GetLastId();
	
	@Modifying
	@Query(value = "INSERT INTO Diagnostico (Id, enfermedad, valoracionEspecialista) VALUES (?,'', '' )", nativeQuery= true)
	public void InsertDiagnostico(long Id);
	
	@Modifying
	@Query(value ="INSERT INTO medicopaciente (medico_Id, paciente_Id) VALUES (?1,?2)", nativeQuery=true)
	public void InsertIntoMedicoPaciente(long M_Id, long P_Id);
	
	@Query(value="select count(*) from medicopaciente where medico_id=?1 AND paciente_id=?2", nativeQuery=true)
	public long RelationExists(long M_Id, long P_Id);
	
	@Query("select count(*) from Cita where MedicoId=?1 and PacienteId=?2")
	public long num_citas(long M_Id, long P_Id);
	
	@Modifying
	@Query(value ="DELETE FROM medicopaciente WHERE medico_id=?1 AND paciente_id=?2", nativeQuery=true)
	public void DeleteManyToMany(long M_Id, long P_Id);
}