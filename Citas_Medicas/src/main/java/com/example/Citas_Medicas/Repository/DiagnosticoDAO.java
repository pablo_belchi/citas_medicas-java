package com.example.Citas_Medicas.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Citas_Medicas.Modelo.Diagnostico;

@Repository
public interface DiagnosticoDAO extends JpaRepository<Diagnostico, Long> {

}