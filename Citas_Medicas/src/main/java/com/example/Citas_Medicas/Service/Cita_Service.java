package com.example.Citas_Medicas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Citas_Medicas.Modelo.Cita;
import com.example.Citas_Medicas.Repository.CitaDAO;

@Service
@Transactional
public class Cita_Service {
	
	@Autowired
	private CitaDAO Repository_;

	public List<Cita> GetAll(){ return Repository_.findAll(); }
	
	public Cita GetById(Long Id) {
		if(Repository_.existsById(Id)) {
			return Repository_.findById(Id).get();
		}
		return new Cita();
	}
	
	public void saveEntity(Cita c) {
		if(Repository_.RelationExists(c.Get_MedicoId(), c.Get_PacienteId())==0) {
			Repository_.InsertIntoMedicoPaciente(c.Get_MedicoId(), c.Get_PacienteId()); // Insertar relación manytomany
		}
		
		Repository_.save(c);
		InsertDiagnostico(); //Insertar diagnostico con Id = Cita_Id
	}
	
	public void InsertDiagnostico() {
		long Id = Repository_.GetLastId();
		Repository_.InsertDiagnostico(Id);
	}
	
	public void DeleteById(Long Id) {
		if(Repository_.existsById(Id)) {
			Cita c = Repository_.findById(Id).get();

			if(Repository_.num_citas(c.Get_MedicoId(), c.Get_PacienteId())==1) { //Eliminar relaciones manytomany
				Repository_.DeleteManyToMany(c.Get_MedicoId(), c.Get_PacienteId());
			}
			Repository_.deleteById(Id); 
			
		} 
	}
	
	@Modifying 
	public void Modify(Cita c) {
		if(Repository_.existsById(c.Get_Id())) {
			Repository_.save(c);
		}
	}
		
}