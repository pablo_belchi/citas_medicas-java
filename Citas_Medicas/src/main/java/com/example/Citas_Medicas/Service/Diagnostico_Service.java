package com.example.Citas_Medicas.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.example.Citas_Medicas.Modelo.Diagnostico;
import com.example.Citas_Medicas.Repository.CitaDAO;
import com.example.Citas_Medicas.Repository.DiagnosticoDAO;

@Service
@Transactional
public class Diagnostico_Service {

	@Autowired
	private DiagnosticoDAO Repository_;
	@Autowired
	private CitaDAO CRepository_;
	
	public List<Diagnostico> GetAll(){ return Repository_.findAll(); }
	
	public Diagnostico GetById(Long Id) {
		if(Repository_.existsById(Id)) {
			return Repository_.findById(Id).get();
		}
		
		return new Diagnostico();
	}
	
	public void DeleteById(Long Id) {
		if(Repository_.existsById(Id)) {
			Repository_.deleteById(Id);
		}
	}
	
	@Modifying
	public void Modify(Diagnostico d) {
		if(Repository_.existsById(d.Get_Id())) { //Permitir modificaciones unicamente si la fecha de la cita ya ha pasado
			String fechaActual = LocalDateTime.now().toString();
			Date fechaCita = CRepository_.findById(d.Get_Id()).get().Get_fechaHora().getTime();
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			String form_Cita = formato.format(fechaCita);
			
			String[] arrayFechaActual = fechaActual.split("-");
			int dia = Integer.parseInt(arrayFechaActual[2].substring(0, 2));
			int mes = Integer.parseInt(arrayFechaActual[1]);
			int año = Integer.parseInt(arrayFechaActual[0]);
			
			String[] arrayFechaCita = form_Cita.split("-");
			int diaCita = Integer.parseInt(arrayFechaCita[2].substring(0, 2));
			int mesCita = Integer.parseInt(arrayFechaCita[1]);
			int añoCita = Integer.parseInt(arrayFechaCita[0]);
			
			if((dia >= diaCita && mes >= mesCita && año >= añoCita) || (mes > mesCita && año >= añoCita)
					|| (año > añoCita)) {
				
				Repository_.save(d);
			}
			
		}
	}
	
}
