package com.example.Citas_Medicas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Citas_Medicas.Modelo.Cita;
import com.example.Citas_Medicas.Modelo.Medico;
import com.example.Citas_Medicas.Modelo.Paciente;
import com.example.Citas_Medicas.Repository.MedicoDAO;

@Service
@Transactional
public class Medico_Service {

	@Autowired
	private MedicoDAO Repository_;
	
	public List<Medico> GetAll() { return Repository_.findAll(); }

	public Medico GetById(Long Id) { 
		if(Repository_.existsById(Id)) {
			return Repository_.findById(Id).get();
			
		}
		
		return new Medico();
	}

	public void DeleteById(Long Id) { 
		if(Repository_.existsById(Id)) {
			Repository_.deleteById(Id);
			
		} 
	}
	
	public void saveEntity(Medico m) { 
			Repository_.save(m);	
	}
	
	@Modifying 
	public void Modify(Medico m) { 
		if(Repository_.existsById(m.Get_Id())) {
			List<Cita> citas = Repository_.findById(m.Get_Id()).get().Get_Citas();
			List<Paciente> pacientes =  Repository_.findById(m.Get_Id()).get().Get_Pacientes();
			m.Set_Citas(citas);
			m.Set_Pacientes(pacientes);
			
			Repository_.save(m); 
		}
	}
}