package com.example.Citas_Medicas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Citas_Medicas.Modelo.Paciente;
import com.example.Citas_Medicas.Repository.PacienteDAO;

@Service
@Transactional
public class Paciente_Service {

	@Autowired
	private PacienteDAO Repository_;
	
	public List<Paciente> GetAll() { 

			return Repository_.findAll();
	}
	
	public Paciente GetById(Long Id) { 
		if(Repository_.existsById(Id)) {
			return Repository_.findById(Id).get();
			
		}
		
		return new Paciente();
	}

	public void DeleteById(Long Id) { 
		if(Repository_.existsById(Id)) {
			Repository_.deleteById(Id); 
			
		} 
	}
	
	public void saveEntity(Paciente p) { 
			Repository_.save(p);	
	}
	
	@Modifying 
	public void Modify(Paciente p) { 
		if(Repository_.existsById(p.Get_Id())) {
			Repository_.save(p); 
			
		}
	}
}
