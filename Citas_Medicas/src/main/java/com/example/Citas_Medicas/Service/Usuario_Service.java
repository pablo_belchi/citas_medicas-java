package com.example.Citas_Medicas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Citas_Medicas.Modelo.Usuario;
import com.example.Citas_Medicas.Repository.UsuarioDAO;

@Service
@Transactional
public class Usuario_Service {

	@Autowired
	private UsuarioDAO Repository_;
	
	public List<Usuario> GetAll() { return Repository_.findAll(); }

	public Usuario GetById(Long Id) { 
		if(Repository_.existsById(Id)) {
			return Repository_.findById(Id).get();
		}
		
		return new Usuario();
	} 

	public void DeleteById(Long Id) { 
		if(Repository_.existsById(Id)) {
			Repository_.deleteById(Id);
			
		}
	}
	
	@Modifying 
	public void Modify(Usuario u) { 
		
		if(Repository_.existsById(u.Get_Id())) {
			Repository_.findById(u.Get_Id()).get().Set_nombre(u.Get_nombre());
			Repository_.findById(u.Get_Id()).get().Set_apellidos(u.Get_apellidos());
			Repository_.findById(u.Get_Id()).get().Set_clave(u.Get_clave());
			Repository_.findById(u.Get_Id()).get().Set_usuario(u.Get_usuario());
			
			Repository_.save(Repository_.findById(u.Get_Id()).get());
		}		
	}
	
}